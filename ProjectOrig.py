
"""
@author: Amin
"""
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
# Pandas for managing datasets
import pandas as pd
import os


# """ This code creates a number of empty folders (N) in the directory where
# this py file is located. Then inside each Folder it creates 3 different
#  txt file which containes the data for 3 functions (sin(Nx), cos(Nx) and sqrt(Nx)).
# The goal is to plot the graph for the functions that are listed in the
#  a plotlist. To do this the code goes inside each folder and looks the list
#  of functions that we listed in the plotlist txt file and finally it plots
# existed files in the folders.

#

cwd = os.getcwd()
cwd12 = os.getcwd()


def creat_folder(N):  # N = Number_of_folders
    CreatNewFolder = r'Folder_' + str(N)
    if not os.path.exists(CreatNewFolder):
        os.makedirs(CreatNewFolder)
    return


N = 5  # Number_of_folders
for i in range(1, N+1):
    creat_folder(i)


def creat_sin(i):
    cwd = os.getcwd()
    dest = cwd + '\Folder_' + str(i)
    file_name = 'sin(' + str(i) + 'x)' + '.txt'
    completeName = os.path.join(dest, file_name)
    file = open(completeName, "w")
    for j in range(100):
        sin = np.sin(i*j*0.1)
        mot = str(j)+'   '+str(sin)+'   ' + '\n'
        file.write(mot)
    file.close()

    return


def creat_cos(i):
    cwd = os.getcwd()
    dest = cwd + '\Folder_' + str(i)
    file_name = 'cos(' + str(i) + 'x)' + '.txt'
    completeName = os.path.join(dest, file_name)
    file = open(completeName, "w")
    for j in range(100):
        cos = np.cos(i*j*0.1)
        mot = str(j)+'   '+str(cos)+'   ' + '\n'
        file.write(mot)
    file.close()

    return


def creat_sqrt(i):
    cwd = os.getcwd()
    dest = cwd + '\Folder_' + str(i)
    file_name = 'sqrt(' + str(i) + 'x)' + '.txt'
    completeName = os.path.join(dest, file_name)
    file = open(completeName, "w")
    for j in range(100):
        sqrt = np.sqrt(i*j*0.1)
        mot = str(j)+'   '+str(sqrt)+'   ' + '\n'
        file.write(mot)
    file.close()

    return


##########___________________ Creating plot List in a txt file ___________________############
plotlist = []
file = open('plotlist.txt', 'w')


def plot_list(i):
    file = open('plotlist.txt', 'a')
    name = 'sin('+str(i)+'x).txt'+'\n'
    file.write(name)
    name = 'cos('+str(i)+'x).txt'+'\n'
    file.write(name)
    name = 'sqrt('+str(i)+'x).txt'+'\n'
    file.write(name)
    file.close()

    return


file.close()


#########_____________________________________________________##########
for i in range(1, 6):
    creat_sin(i)
    creat_cos(i)
    creat_sqrt(i)
    plot_list(i)


#### _________________________ Reading Wanted_List __________________________________#####
wanted_list = []
file = open("plotlist.txt", "r")
for line in file:
    ee = line.split()
    for i in range(len(ee)):
        ee[i] = str(ee[i])
        wanted_list.append(ee[i])
##----------------------------------------------------------------##


def file_existance_checker(N):
    final_list = []
    cwd = os.getcwd()
    dest = cwd + '\Folder_' + str(N)

    # List of existed files in directory ###
    existed_list = os.listdir(f'{str(dest)}')

    for k in range(len(wanted_list)):
        for j in range(len(existed_list)):
            if wanted_list[k] == existed_list[j]:

                final_list.append(wanted_list[k])

    return len(final_list), final_list


####----------------- File Reader ------------------######


def file_reader(file_name):
    CulumnNumbers = 2
    All = np.zeros([100, CulumnNumbers])
    file_name = str(file_name)  # + '.txt'
    # print('file_name',file_name)
    # input()
    nn = -1
    with open(file_name) as file:

        yy = file.readlines()
        for kh in yy:
            nn = int(nn+1)
            line = kh.split()

            All[nn, 0] = int(line[0])  # Al[nn,Number of Culumns in the file]
            All[nn, 1] = line[1]
    return All[:, 0], All[:, 1]


###----------------------------------------------------------------------------###
def plotter(x, y):

    plt.plot(x, y, "b", label="Gauges", color="red",
             marker=".", linestyle='dotted', lw=1.0)
    plt.xlabel("X ", fontsize=10)
    plt.ylabel("Y ", fontsize=10)
    # plt.title("t'= 14.42", fontsize=10)
    #plt.legend(loc='lower right')
    plt.grid(b=True, which='major', color='gray', alpha=0.6,
             linestyle='dotted', lw=1.0)

    plt.show()

    return


#
cwd = os.getcwd()
# cwd2 = os.getcwd()
for i in range(1, N+1):
    n, functions = file_existance_checker(i)
    #print(n, functions)

    #print('first Dir cwd12', cwd12)
    for j in range(len(functions)):
        #print(j, '', functions[j])
        functions[j] = str(functions[j])

        dest = cwd + '\Folder_' + str(i)
        os.chdir(dest)

        #print('Dir cwd', dest)
        x, y = file_reader(functions[j])
        os.chdir(cwd12)

        plotter(x, y)
